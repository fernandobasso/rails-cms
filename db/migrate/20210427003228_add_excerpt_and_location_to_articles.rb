class AddExcerptAndLocationToArticles < ActiveRecord::Migration[6.1]
  def change
    add_column :articles, :excerpt, :string, limit: 512
    add_column :articles, :location, :string, limit: 256
  end
end
