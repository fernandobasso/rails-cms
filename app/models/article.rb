class Article < ApplicationRecord
  validates :title, :body, presence: true
  # validates(:title, :body, { :presence => true })
end
