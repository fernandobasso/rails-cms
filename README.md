# Rails CMS

## Run

```bash
source env.sh ./config/envfiles/dev.rails.env --print
docker-compose up --build
```

Then, access the application at http://localhost:3000

